# iptlogtool

## Rationale

This program reads information generated by the LOG target of iptables and processes it as required. Originally, this job was done by a bash script I wrote back in 2016 and I wanted to have a solution that is better to maintain and works better (there have been issues with the bash script). Apart from this I wanted to learn the Vala programming language and due to its simple nature, this program seemed right to me as a training project.

## Building, Running, Testing

This program is written using the Vala programming language. It uses Meson for building.

After cloning the repository, execute

```
rm -rf build/; meson setup build;
```

to setup the build directory. After that

```
ninja -C build/
```

can be used to build the program and

```
build/src/iptlogtool
```

to run the program. Unit tests can be executed by

```
ninja test -C build/
```
