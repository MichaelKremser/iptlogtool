class LogLine : GLib.Object
{
    public double uptime { get; private set; }

    public string inInterface { get; private set; }

    public string outInterface { get; private set; }

    public string mac { get; private set; }

    public string srcAddress { get; private set; }

    public string dstAddress { get; private set; }

    public int len { get; private set; }

    public uint tos { get; private set; }

    public uint prec { get; private set; }

    public int ttl { get; private set; }

    public int id { get; private set; }

    public string protocol { get; private set; }

    public ushort srcPort { get; private set; }

    public ushort dstPort { get; private set; }

    public uint window { get; private set; }

    public ushort tc { get; private set; }

    public ushort hopLimit { get; private set; }

    public uint flowLabel { get; private set; }

    public ushort res { get; private set; }

    public uint urgp { get; private set; }

    public LogLine(double uptime, string inInterface, string outInterface, string mac, string srcAddress, string dstAddress, int len, uint tos, uint prec, int ttl, int id, string protocol, ushort srcPort, ushort dstPort, uint window, ushort tc, ushort hopLimit, uint flowLabel, ushort res, uint urgp)
    {
        this.uptime = uptime;
        this.inInterface = inInterface;
        this.outInterface = outInterface;
        this.mac = mac;
        this.srcAddress = srcAddress;
        this.dstAddress = dstAddress;
        this.len = len;
        this.tos = tos;
        this.prec = prec;
        this.ttl = ttl;
        this.id = id;
        this.protocol = protocol;
        this.srcPort = srcPort;
        this.dstPort = dstPort;
        this.window = window;
        this.tc = tc;
        this.hopLimit = hopLimit;
        this.flowLabel = flowLabel;
        this.res = res;
        this.urgp = urgp;
    }
}