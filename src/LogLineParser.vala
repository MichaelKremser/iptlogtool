class LogLineParser : GLib.Object
{
    public LogLineParser()
    {
    }

    public LogLine Parse(string line)
    {
        double uptime = 0;
        string inInterface = "", outInterface = "", mac = "", srcAddress = "", dstAddress = "", protocol = "";
        int len = 0, ttl = 0, id = 0;
        uint tos = 0, prec = 0, window = 0, urgp = 0, flowLabel = 0;
        ushort sourcePort = 0, destinationPort = 0, res = 0, tc = 0, hopLimit = 0;
        foreach (var item in line.split(" "))
        {
            if (StringTools.startsWith(item,"IN="))
            {
                inInterface = item.substring(3);
            }
            else if (StringTools.startsWith(item, "OUT="))
            {
                outInterface = item.substring(4);
            }
            else if (StringTools.startsWith(item, "MAC="))
            {
                mac = item.substring(4);
            }
            else if (StringTools.startsWith(item, "SRC="))
            {
                srcAddress = item.substring(4);
            }
            else if (StringTools.startsWith(item, "DST="))
            {
                dstAddress = item.substring(4);
            }
            else if (StringTools.startsWith(item, "PROTO="))
            {
                protocol = item.substring(6);
            }
            else if (StringTools.startsWith(item, "LEN="))
            {
                len = int.parse(item.substring(4));
            }
            else if (StringTools.startsWith(item, "TTL="))
            {
                ttl = int.parse(item.substring(4));
            }
            else if (StringTools.startsWith(item, "WINDOW="))
            {
                window = uint.parse(item.substring(7));
            }
            else if (StringTools.startsWith(item, "URGP="))
            {
                urgp = uint.parse(item.substring(5));
            }
            else if (StringTools.startsWith(item, "FLOWLBL="))
            {
                flowLabel = uint.parse(item.substring(8));
            }
            else if (StringTools.startsWith(item, "SPT="))
            {
                sourcePort = (ushort)uint.parse(item.substring(4));
            }
            else if (StringTools.startsWith(item, "DPT="))
            {
                destinationPort = (ushort)uint.parse(item.substring(4));
            }
            else if (StringTools.startsWith(item, "RES="))
            {
                res = (ushort)uint.parse(item.substring(4));
            }
            else if (StringTools.startsWith(item, "TC="))
            {
                tc = (ushort)uint.parse(item.substring(3));
            }
            else if (StringTools.startsWith(item, "HOPLIMIT="))
            {
                hopLimit = (ushort)uint.parse(item.substring(9));
            }
            else if (StringTools.startsWith(item, "ID="))
            {
                id = int.parse(item.substring(3));
            }
            else if (StringTools.startsWith(item, "TOS=0x"))
            {
                tos = (uint)parseHexNumber(item.substring(6));
            }
            else if (StringTools.startsWith(item, "PREC=0x"))
            {
                prec = (uint)parseHexNumber(item.substring(7));
            }
            else if (item[0] == '[' && item[item.length - 1] == ']')
            {
                uptime = double.parse(item.substring(1, item.length - 2));
            }
        }
    
        var logLine = new LogLine(uptime, inInterface, outInterface, mac, srcAddress, dstAddress, len, tos, prec, ttl, id, protocol, sourcePort, destinationPort, window, tc, hopLimit, flowLabel, res, urgp);
        return logLine;
    }

    private ulong parseHexNumber(string hexNumber)
    {
        var digitMultiplier = 1;
        var totalSum = 0;
        for (var i = 1; i <= hexNumber.length; i++)
        {
            var digit = hexNumber[hexNumber.length - i].to_string().down();
            var currentValue = "0123456789abcdef".index_of(digit) * digitMultiplier;
            totalSum += currentValue;
            digitMultiplier *= 16;
        }
        return totalSum;
    }
}