public class StringTools
{
    public static bool startsWith(string s, string start)
    {
        return (s.length >= start.length && s.substring(0, start.length) == start);
    }
}