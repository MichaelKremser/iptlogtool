void main(string[] args)
{
    var parser = new LogLineParser();
    var logLine = parser.Parse(args[1]);
    print(logLine.inInterface + "\n");
    print(logLine.outInterface + "\n");
}