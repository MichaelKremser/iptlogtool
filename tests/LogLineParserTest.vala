using GLib;

public void Ctor_NoArguments_ReturnsObject()
{
    var logLineParser = new LogLineParser();
    
    assert(logLineParser != null);
}

public void Parse_ValidLine_DataIsParsedCorrectly()
{
    // Arrange
    var logLineParser = new LogLineParser();
    var testLine = "Aug 25 07:37:54 host kernel: [230149.885426] NASTY IN=eth3 OUT=eth4 MAC=ff:01:02:03:04:05:06:07:08:09:0a:0b:0c:0d SRC=179.0.113.219 DST=127.154.224.1 LEN=60 TOS=0x7F PREC=0xCA TTL=58 ID=16507 DF PROTO=TCP SPT=11044 DPT=23 WINDOW=28300 RES=0x82 SYN URGP=3 TC=9 HOPLIMIT=56 FLOWLBL=875630";
    
    // Act
    var logLine = logLineParser.Parse(testLine);

    // Assert
    assert(logLine.inInterface == "eth3");
    assert(logLine.outInterface == "eth4");
    assert(logLine.mac == "ff:01:02:03:04:05:06:07:08:09:0a:0b:0c:0d");
    assert(logLine.srcAddress == "179.0.113.219");
    assert(logLine.dstAddress == "127.154.224.1");
    assert(logLine.len == 60);
    assert(logLine.tos == 127);
    assert(logLine.prec == 202);
    assert(logLine.ttl == 58);
    print(logLine.id.to_string() + "\n");
    assert(logLine.id == 16507);
    assert(logLine.protocol == "TCP");
    assert(logLine.srcPort == 11044);
    assert(logLine.dstPort == 23);
    assert(logLine.window == 28300);
    assert(logLine.res == 130);
    assert(logLine.urgp == 3);
    assert(logLine.tc == 9);
    assert(logLine.hopLimit == 56);
    assert(logLine.flowLabel == 875630);
}

public int main(string[] args)
{
    Test.init(ref args);

    Test.add_func("/Ctor_NoArguments_ReturnsObject", Ctor_NoArguments_ReturnsObject);
    Test.add_func("/Parse_ValidLine_DataIsParsedCorrectly", Parse_ValidLine_DataIsParsedCorrectly);
    return Test.run ();
}